'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /map when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/map");
  });


  describe('viewMap', function() {

    beforeEach(function() {
      browser.get('index.html#/map');
    });


    it('should work', function() {
      var input = element(by.model('vm.newWaypointTitle'));
      var waypoints = element.all(by.repeater('waypoint in vm.waypoints'));

      expect($('script[src*="https://api-maps.yandex.ru"]').isPresent()).toBe(true);

      expect(waypoints.count()).toEqual(0);

      input.sendKeys('foo');
      input.submit();

      expect(waypoints.count()).toEqual(1);

      input.sendKeys('bar');
      input.submit();

      expect(waypoints.count()).toEqual(2);

      expect(waypoints.get(0).getText()).toContain('foo');
      expect(waypoints.get(1).getText()).toContain('bar');

      // change the order of waypoints
      browser.actions()
        .mouseDown(waypoints.get(0))
        .mouseMove(waypoints.get(1))
        .mouseUp()
        .perform();

      expect(waypoints.get(0).getText()).toContain('bar');
      expect(waypoints.get(1).getText()).toContain('foo');

      // remove the first waypoint
      waypoints.get(0).element(by.tagName('button')).click();

      expect(waypoints.count()).toEqual(1);
      expect(waypoints.get(0).getText()).toContain('foo');
    });

  });
});

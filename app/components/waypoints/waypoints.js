'use strict';

angular.module('myApp.waypoints', [])

.factory('waypoints', function() {
  var waypoints = [];

  var service = {
    waypoints: waypoints,
    add: add,
    remove: remove,
    getCoordinates: getCoordinates
  };

  return service;

  function add(title, coordinates) {
    var waypoint = {};
    waypoint.title = title;
    waypoint.coordinates = coordinates || [];

    waypoints.push(waypoint);
  }

  function remove(index) {
    waypoints.splice(index, 1);
  }

  function getCoordinates() {
    var coordinates = [];
    angular.forEach(waypoints, function(waypoint) {
      coordinates.push(waypoint.coordinates);
    });
    return coordinates;
  }
});

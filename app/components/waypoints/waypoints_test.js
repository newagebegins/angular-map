'use strict';

describe('myApp.waypoints module', function() {

  beforeEach(module('myApp.waypoints'));

  describe('waypoints service', function(){
    var waypoints;

    beforeEach(inject(function(_waypoints_) {
      waypoints = _waypoints_;
    }));

    it("doesn't have waypoints initially", function() {
      expect(waypoints.waypoints.length).toEqual(0);
    });

    it("adds waypoints", function() {
      waypoints.add('Test waypoint');
      expect(waypoints.waypoints.length).toEqual(1);
    });

    it("removes waypoints", function() {
      waypoints.add('Test waypoint');
      expect(waypoints.waypoints.length).toEqual(1);
      waypoints.remove(0);
      expect(waypoints.waypoints.length).toEqual(0);
    });

    it("should provides an array of all waypoint coordinates", function() {
      waypoints.add('Test waypoint 1', [1,2]);
      waypoints.add('Test waypoint 2', [3,4]);
      expect(waypoints.getCoordinates()).toEqual([[1,2], [3,4]]);
    });

  });
});

'use strict';

angular.module('myApp.map.map-directive', [])

.directive('myMap', ['$document', '$window', 'waypoints', function($document, $window, waypoints) {
  var directive = {
    link: link,
    restrict: 'A',
    scope: {
      waypoints: '=myMap'
    }
  };

  return directive;

  function link(scope, elm, attrs) {
    var script = $document[0].createElement('script');
    script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=myMapInit';
    $document[0].body.appendChild(script);

    var map;
    var waypointLine = null;

    $window.myMapInit = function(ymaps) {
      map = new ymaps.Map(elm[0], {
        center: [55.72, 37.44],
        zoom: 10
      });

      updateWaypoints(waypoints.waypoints);
      scope.$watchCollection('waypoints', updateWaypoints);
    };

    function updateWaypoints(newWaypoints) {
      map.geoObjects.removeAll();

      angular.forEach(newWaypoints, function(waypoint) {
        if (waypoint.coordinates.length === 0) {
          waypoint.coordinates = map.getCenter();
        }

        var placemark = new ymaps.Placemark(waypoint.coordinates, {
          balloonContent: waypoint.title
        },{
          preset: 'islands#circleIcon',
          draggable: true
        });

        map.geoObjects.add(placemark);

        placemark.geometry.events.add('change', function(event) {
          waypoint.coordinates = placemark.geometry.getCoordinates();
          waypointLine.geometry.setCoordinates(waypoints.getCoordinates());
        });
      });

      waypointLine = new ymaps.Polyline(waypoints.getCoordinates(), {}, {
        strokeWidth: 4
      });

      map.geoObjects.add(waypointLine);
    }
  }
}]);

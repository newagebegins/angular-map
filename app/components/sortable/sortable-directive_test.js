'use strict';

describe('myApp.sortable.sortable-directive module', function() {
  beforeEach(module('myApp.sortable.sortable-directive'));

  describe('controller', function() {
    it('should update model array when item is moved', function() {
      inject(function($compile, $rootScope) {
        var scope = $rootScope.$new();

        scope.items = [1, 2, 3];

        var element = angular.element('<ul my-sortable="items"></ul>');
        $compile(element)(scope);
        scope.$digest();

        var controller = element.controller('mySortable');

        controller.onChangePosition(0, 1);
        expect(scope.items).toEqual([2, 1, 3]);

        controller.onChangePosition(2, 0);
        expect(scope.items).toEqual([3, 2, 1]);
      });
    });
  });
});

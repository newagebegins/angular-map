'use strict';

angular.module('myApp.sortable.sortable-item-directive', [])

.directive('mySortableItem', ['$document', function($document) {
  var directive = {
    link: link,
    restrict: 'A',
    require: '^^mySortable',
    scope: {
      itemIndex: '=mySortableItem'
    }
  };

  return directive;

  function link(scope, elm, attrs, sortableCtrl) {
    var activeItem;
    elm.on('mousedown', mousedown);

    function mousedown(event) {
      if (event.target.tagName === 'BUTTON') {
        return;
      }

      event.preventDefault();
      activeItem = angular.element(this);
      activeItem.addClass('is-active');

      $document.on('mousemove', mousemove);
      $document.on('mouseup', mouseup);
    }

    function mousemove(event) {
      var itemElements = elm.parent().children();

      for (var itemIndex = 0; itemIndex < itemElements.length; ++itemIndex) {
        var itemEl = itemElements[itemIndex];

        if (itemEl === activeItem[0]) {
          continue;
        }

        var rect = itemEl.getBoundingClientRect();
        // if mouse cursor is inside the element
        if (event.pageX > rect.left && event.pageX < rect.right &&
            event.pageY > rect.top && event.pageY < rect.bottom)
        {
          sortableCtrl.onChangePosition(scope.itemIndex, itemIndex);
          break;
        }
      }
    }

    function mouseup() {
      activeItem.removeClass('is-active');
      $document.off('mousemove', mousemove);
      $document.off('mouseup', mouseup);
    }
  }
}]);

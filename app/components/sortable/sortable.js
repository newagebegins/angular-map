'use strict';

angular.module('myApp.sortable', [
  'myApp.sortable.sortable-directive',
  'myApp.sortable.sortable-item-directive'
]);

'use strict';

angular.module('myApp.sortable.sortable-directive', [])

.directive('mySortable', [function() {
  var directive = {
    restrict: 'A',
    scope: {
      items: '=mySortable'
    },
    controller: SortableController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;

  SortableController.$inject['$scope'];

  function SortableController($scope) {
    var vm = this;

    vm.onChangePosition = onChangePosition;

    function onChangePosition(currentIndex, newIndex) {
      $scope.$apply(function() {
        // Move item to the new position.
        var item = vm.items[currentIndex];
        vm.items.splice(currentIndex, 1);
        vm.items.splice(newIndex, 0, item);
      });
    }
  }
}]);

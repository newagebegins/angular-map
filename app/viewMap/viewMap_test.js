'use strict';

describe('myApp.viewMap module', function() {

  beforeEach(module('myApp.viewMap'));

  describe('viewMap controller', function(){
    var controller;

    beforeEach(inject(function($controller) {
      controller = $controller('ViewMapCtrl');
    }));

    it("doesn't have waypoints initially", function() {
      expect(controller.waypoints.length).toEqual(0);
    });

    it("doesn't add waypoints with empty titles", function() {
      controller.addWaypoint();
      expect(controller.waypoints.length).toEqual(0);
    });

    it("adds waypoints with filled titles", function() {
      controller.newWaypointTitle = 'Test waypoint';
      controller.addWaypoint();
      expect(controller.waypoints.length).toEqual(1);
    });

    it("empties input title after adding a waypoint", function() {
      controller.newWaypointTitle = 'Test waypoint';
      controller.addWaypoint();
      expect(controller.newWaypointTitle).toEqual('');
    });

    it("removes waypoints", function() {
      controller.newWaypointTitle = 'Test waypoint';
      controller.addWaypoint();
      expect(controller.waypoints.length).toEqual(1);
      controller.removeWaypoint(0);
      expect(controller.waypoints.length).toEqual(0);
    });

  });
});

'use strict';

angular.module('myApp.viewMap', ['ngRoute', 'myApp.waypoints'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/map', {
    templateUrl: 'viewMap/viewMap.html',
    controller: 'ViewMapCtrl',
    controllerAs: 'vm'
  });
}])

.controller('ViewMapCtrl', ['waypoints', function(waypoints) {
  var vm = this;

  vm.waypoints = waypoints.waypoints;
  vm.newWaypointTitle = '';
  vm.addWaypoint = addWaypoint;
  vm.removeWaypoint = removeWaypoint;

  function addWaypoint() {
    if (vm.newWaypointTitle === '') {
      return;
    }

    waypoints.add(vm.newWaypointTitle);
    vm.newWaypointTitle = '';
  }

  function removeWaypoint(waypointIndex) {
    waypoints.remove(waypointIndex);
  }
}]);

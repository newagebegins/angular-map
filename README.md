За базу был взят проект [angular-seed](https://github.com/angular/angular-seed).  Более подробную документацию можно посмотреть там.

## Установка зависимостей

Убедитесь, что у Вас установлен nodejs и npm ([http://nodejs.org/](http://nodejs.org/)).  Чтобы установить зависимости, выполните:

```
npm install
```

## Запуск приложения

```
npm start
```

Приложение будет доступно по адресу `http://localhost:8000/app/index.html`

## Запуск unit-тестов

```
npm test
```

## Запуск end-to-end тестов

```
npm run protractor
```
